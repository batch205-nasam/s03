// To create a class in JS, we use the class keyword and {}
// Naming convention for classes: Begins with Uppercase Characters

/*
	Syntax:
	
	class Name {
	
	};

*/

class Dog {

	// We add a constructor method to a class to be able to initialize values upon instantiation of an object from a class
	// "Instantiation" is the technical term when cration an object from a class
	// An object created from a class is called an instance
	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
}

// Instantiate a new object from the class
// new keyword is used to instatiate an object from a class


let dog1 = new Dog("Bantay","chihuahua",3);

console.log(dog1);

/*
	Create a new class called Person

	Instantiate with the ff fields:

	name <string>
	age	<number>
	nationality <string>
	address <string>

	Instiate 2 new objects from the Person class as person1 and person2.

	Log both objects in the console. Take a screenshot of your console and send to hangouts
*/

class Person {

	constructor(name, age, nationality, address){

		this.name = name;
		this.age = age;
		this.nationality = nationality;
		this.address = address;

		if(typeof age !== "number"){
			this.age = undefined;
		} else {
			this.age = age;
		}
	}
		greet(){
			console.log("Hello! Good Morning!");
			// this must be returned if the intended method can be chained
			return this;
		};
		introduce(){
			// we should be able to return 'this', so that when used in a chain, we can still have access to the reference of 'this'
			console.log(`Hi! My name is ${this.name}`)
			return this;
		};
		changeAddress(newAddress){
			this.address = newAddress;
			return this;
		}
}

let person1 = new Person("Kang Seulgi", 28, "Korean","Seoul, Korea");
let person2 = new Person("Bae Joohyun", 31, "Korean","Seoul, Korea");

console.log(person1);
console.log(person2);

// You can actually chain methods from a instance
person1.greet().introduce();

class Student {

	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.grades = grades;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		grades.every(grade => {
			if(typeof grade !== "number"){
				return this.grades = undefined;
			} else{
				return this.grades = grades;
			}
		});
		}
	login(){
    	console.log(`${this.email} has logged in`);   
	};
	logout(){
        console.log(`${this.email} has logged out`);	  
	};
	listGrades(){
        this.grades.forEach(grade => {   
        console.log(grade);
		})
	};
	computeAve(){
	  	let sum = this.grades.reduce((accumulator,num) => {
	    return accumulator += num;
	    })
	    this.average = sum/4;
	    return this 
  	};
	willPass(){
		if(this.computeAve() >= 85){
			this.isPassed = true;
			return this
		} else {
			this.isPassed = false;
			return this
		}
	};
	willPassWithHonors(){
		if(this.average >= 90){
			this.isPassedWithHonors = true;
			return this
		} else if(this.average >= 85 && this.average < 90){
			this.isPassedWithHonors = false;
			return this
		} else if(this.average < 85) {
			return this
		}
	};
}

let student1 = new Student("John","john@gmail.com",[89, 84, 78, 88]);
console.log(student1);


let student2 = new Student("Joe","joe@gmail.com",[78, 82, 79, 85])
console.log(student2);

let student3 = new Student("Jane","jane@gmail.com",[87, 89, 91, 93])
console.log(student3);

let student4 = new Student("Jessie","jessie@gmail.com",[91, 89, 92, 93])
console.log(student4);

